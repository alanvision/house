package com.five.service.impl;

import com.five.common.DataGridView;
import com.five.domain.Sale;
import com.five.mapper.SaleMapper;
import com.five.service.SaleService;
import com.five.vo.SaleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SaleServiceImpl implements SaleService {

    @Autowired
    private SaleMapper saleMapper;

    //查询类别信息
    @Override
    public List<Sale> queryAllSale(SaleVo saleVo){
        List<Sale> data = saleMapper.queryAllSale(saleVo);
        return data;
    }

    //导航栏查询
    public List<Sale> querySaleByUse(SaleVo saleVo){
        List<Sale> data = saleMapper.querySaleByUse(saleVo);
        return data;
    }

    //添加类别信息
    @Override
    public DataGridView addSale(SaleVo saleVo){
        DataGridView dataGridView = new DataGridView();
        if(!saleMapper.queryAllSale(saleVo).isEmpty()){
            dataGridView.setCode(500);
            dataGridView.setMsg("类别已存在！！");
        }else {
            saleVo.setAvailable(1);
            saleMapper.alter();
            int row =saleMapper.addSale(saleVo);
            if(row == 0){
                dataGridView.setCode(500);
                dataGridView.setMsg("添加失败，请重新添加！！");
            }else {
                dataGridView.setCode(200);
                dataGridView.setMsg("添加成功！！");
            }
        }
        return dataGridView;
    }

    //修改类别信息
    @Override
    public DataGridView updateSale(SaleVo saleVo){
        DataGridView dataGridView = new DataGridView();
        if(saleMapper.querySaleByName(saleVo.getName()) != null){
            dataGridView.setCode(500);
            dataGridView.setMsg("类别已存在！！");
        }else {
            int row = saleMapper.updateSale(saleVo);
            if(row == 1){
                dataGridView.setCode(200);
                dataGridView.setMsg("修改成功！！");
            }else {
                dataGridView.setCode(500);
                dataGridView.setMsg("修改失败！！");
            }
        }
        return dataGridView;
    }

    //删除类别信息
    @Override
    public DataGridView deleteSale(SaleVo saleVo){
        DataGridView dataGridView = new DataGridView();
        int row = saleMapper.deleteSale(saleVo);
        if(row == 1){
            dataGridView.setMsg("删除成功！！");
            dataGridView.setCode(200);
        }else{
            dataGridView.setCode(500);
            dataGridView.setMsg("删除失败！！");
        }
        return dataGridView;
    }

}
