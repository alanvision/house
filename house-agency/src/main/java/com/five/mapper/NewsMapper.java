package com.five.mapper;

import com.five.domain.News;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface NewsMapper {

    //查询新闻
    List<News> queryAllNews(News news);

    //根据Id查询新闻
    News queryNewById(News news);

    //添加新闻
    void alter();
    int addNews(News news);

    //修改新闻
    int updateNews(News news);

    //删除新闻
    int deleteNews(News news);



}
