package com.five.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class Order {

    private String id;
    private Integer cId;
    private String cName;
    private Double money;
    private Double commission;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    private Integer type;
    private String oId;
    private String oname;
    private String ophone;
    private String aId;
    private String aName;
    private String aphone;
    private String saId;
    private String saName;
    private String saphone;
    private Integer status;
    private String hId;
    private String hTitle;
    private String hlocal;
    private String himg;
    private Double hprice;


    public Order() {
    }

    public Order(String id, Integer cId, String cName, Double money, Double commission, Date time, Integer type, String oId, String oname, String ophone, String aId, String aName, String aphone, String saId, String saName, String saphone, Integer status, String hId, String hTitle, String hlocal, String himg, Double hprice) {
        this.id = id;
        this.cId = cId;
        this.cName = cName;
        this.money = money;
        this.commission = commission;
        this.time = time;
        this.type = type;
        this.oId = oId;
        this.oname = oname;
        this.ophone = ophone;
        this.aId = aId;
        this.aName = aName;
        this.aphone = aphone;
        this.saId = saId;
        this.saName = saName;
        this.saphone = saphone;
        this.status = status;
        this.hId = hId;
        this.hTitle = hTitle;
        this.hlocal = hlocal;
        this.himg = himg;
        this.hprice = hprice;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getcId() {
        return cId;
    }

    public void setcId(Integer cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Double getCommission() {
        return commission;
    }

    public void setCommission(Double commission) {
        this.commission = commission;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getoId() {
        return oId;
    }

    public void setoId(String oId) {
        this.oId = oId;
    }

    public String getOname() {
        return oname;
    }

    public void setOname(String oname) {
        this.oname = oname;
    }

    public String getOphone() {
        return ophone;
    }

    public void setOphone(String ophone) {
        this.ophone = ophone;
    }

    public String getaId() {
        return aId;
    }

    public void setaId(String aId) {
        this.aId = aId;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String getAphone() {
        return aphone;
    }

    public void setAphone(String aphone) {
        this.aphone = aphone;
    }

    public String getSaId() {
        return saId;
    }

    public void setSaId(String saId) {
        this.saId = saId;
    }

    public String getSaName() {
        return saName;
    }

    public void setSaName(String saName) {
        this.saName = saName;
    }

    public String getSaphone() {
        return saphone;
    }

    public void setSaphone(String saphone) {
        this.saphone = saphone;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String gethId() {
        return hId;
    }

    public void sethId(String hId) {
        this.hId = hId;
    }

    public String gethTitle() {
        return hTitle;
    }

    public void sethTitle(String hTitle) {
        this.hTitle = hTitle;
    }

    public String getHlocal() {
        return hlocal;
    }

    public void setHlocal(String hlocal) {
        this.hlocal = hlocal;
    }

    public String getHimg() {
        return himg;
    }

    public void setHimg(String himg) {
        this.himg = himg;
    }

    public Double getHprice() {
        return hprice;
    }

    public void setHprice(Double hprice) {
        this.hprice = hprice;
    }
}



