package com.five.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class News {
    private Integer id;
    private String title;
    private String acter;
    private String content;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date time;
    private Integer available;
    private String image;

    private String aname;

    public News(){
    }

    public News(Integer id, String title, String acter, String content, Date time, Integer available, String image, String aname) {
        this.id = id;
        this.title = title;
        this.acter = acter;
        this.content = content;
        this.time = time;
        this.available = available;
        this.image = image;
        this.aname = aname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getActer() {
        return acter;
    }

    public void setActer(String acter) {
        this.acter = acter;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();;
    }

    public Date getTime() {
        return new Date();
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    public Integer getAvailable() {
        return available;
    }

    public String getAname() {
        return aname;
    }

    public void setAname(String aname) {
        this.aname = aname;
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", acter='" + acter + '\'' +
                ", content='" + content + '\'' +
                ", time=" + time +
                ", available=" + available +
                ", image='" + image + '\'' +
                '}';
    }
}
