package com.five.domain;

// 装修风格
public class Decoration {
    private int id;
    private String style;
    private Integer available;

    public Decoration() {
    }

    public Decoration(int id, String style) {
        this.id = id;
        this.style = style;
    }

    public Decoration(int id, String style, int available) {
        this.id = id;
        this.style = style;
        this.available = available;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public Integer getAvailable() {
        return available;
    }

    public void setAvailable(Integer available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return "Decoration{" +
                "id=" + id +
                ", style='" + style + '\'' +
                ", available=" + available +
                '}';
    }
}
