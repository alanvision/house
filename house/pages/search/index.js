import {request} from "../../request/index.js";

Page({
  data: {
    isChecked:"checked"
  },
  //城市接口参数
  City:{
    name:""
  },
  //搜索
  async formSubmit(house) {
    this.City.name = house.detail.value.cName;
    const res = await request({ url: "/user/house/searchCity", method: "POST", data: this.City,header: { "content-type": "application/x-www-form-urlencoded" } });
    if(res.data.code==200){
      const id = res.data.data.id;
      wx.redirectTo({
        url:"/pages/house_list/index?cId="+id+"&type="+house.detail.value.type
      })
    }else{
      wx.showToast({
        title: res.data.msg,
        icon: 'error',
        mask: true
      });
    }
  },
  //重置
  formReset () {
    this.setData({
      isChecked:"checked"
    })
  }
})